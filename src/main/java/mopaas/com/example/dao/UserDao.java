package mopaas.com.example.dao;
 import java.sql.SQLException;
 import java.util.List;
 import mopaas.com.example.entry.UserEntry;
 public interface UserDao {
    public List<UserEntry> selectUser() throws SQLException;
 } 