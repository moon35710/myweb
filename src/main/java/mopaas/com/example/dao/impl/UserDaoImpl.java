package mopaas.com.example.dao.impl;
 import java.sql.SQLException;
 import java.util.List;
 import mopaas.com.example.dao.UserDao;
 import mopaas.com.example.entry.UserEntry;
 import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
 import org.springframework.stereotype.Repository;
 @Repository
 public class UserDaoImpl extends SqlMapClientDaoSupport implements UserDao {
	@SuppressWarnings("unchecked")
    public List<UserEntry> selectUser() throws SQLException {
        try {
            return (List<UserEntry>) getSqlMapClientTemplate().queryForList(
                "UserEdm.selectAllUsers");
        } catch (org.springframework.dao.DataAccessException e) {
            e.printStackTrace();
        }
        return null;
    }}