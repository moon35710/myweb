package mopaas.com.example.service.imple;
 import java.sql.SQLException;
 import java.util.List;
 import mopaas.com.example.dao.UserDao;
 import mopaas.com.example.entry.UserEntry;
 import mopaas.com.example.service.UserService;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.stereotype.Service;
 @Service
 public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    public List<UserEntry> users() throws SQLException {
        // TODO Auto-generated method stub
        return userDao.selectUser();
    }}