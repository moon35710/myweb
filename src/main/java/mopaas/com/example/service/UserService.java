package mopaas.com.example.service;
 import java.sql.SQLException;
 import java.util.List;
 import mopaas.com.example.entry.UserEntry;
 public interface UserService {
    public List<UserEntry> users() throws SQLException;
 }